<?php

//for API-ablility.
if(isset($_POST['task']) )  {
	$task = $_POST['task'];
	$assignedby = $_SESSION['username'];
	if(!isset($_SESSION['username']) && $_SESSION['admin'] == 1) {
		echo "The admin must be logged in to use the API";
		exit;
	}
	
	$assignedto = $_POST['to'];
	//session class already has a db
	//i should think of something better

	$pub = ($_POST['public']) ? '1' : '0';
	$stmt = "INSERT INTO tasks(id,task,assignedto,assignedby,public,created) 
			 VALUES(NULL,'".$task."','". $assignedto."','" .$assignedby. "','".$pub."','".date('Y-m-d H:i:s')."');";
	
	if($dbconn->query($stmt)) {
	
		if(isset($_GET['submit'])) {
			//was submitted via api
			$message = array('result' => 'success') ;
			echo json_encode($message);
			$_SESSION['api_request'] = true;
			exit;
		} else {
			echo 'Success!';
		}
	} else {
		$_SESSION['api_request'] = false;
		echo 'Error adding a task';
		exit;
	}
} 
if(!$_SESSION['createability']) {
	exit('You do not have the ability to create tasks');
} else {




?>

<form action="?c=<?php echo $controller;?>&f=<?php echo $func; ?>" method="post">
	<table>
		<tr><td>Task:</td><td><input name="task" type="text"></td></tr>
		<tr><td>Assigned by:</td><td><?php echo $_SESSION['username'] ?> (you)</td></tr>
		<tr><td>Assign to:</td>
			<td><select name="to">
				<option value="1" selected="selected">admin</option>
			<?php
			echo	load_users();
			?>			
			</select></td></tr>
			<?php
				if($_SESSION['privateability']) {
					echo '<tr><td>Public?</td>';
					echo '<td><select name="public"><option value="0">no</option><option  selected="selected" value="1" >yes</option></select></td>';
				}
			?>
		<tr><td colspan="2"><input type="submit" value="submit" name="submit"></tr>
	</table>
</form>

<?php
}
?>