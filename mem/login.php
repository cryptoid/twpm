<?php
session_start();
//this page should only be access through index.php

require_once('app/template/header.php');
require_once('app/func/sessions.php');

//a bit hackish. :P
$config['serv'] = $db['default']['hostname'];
$config['dbuser'] = $db['default']['username'];
$config['dbpass'] = $db['default']['password'];
$config['database'] = $db['default']['database'];

?>	<body>
	<div class="container">
		  <div class="row">
		  <div class="span8">
		  <?php
			$uname = $_POST['username'];
		
			$pwd = $_POST['password'];
			
			$sess = new Sessions($uname,$config['serv'],$config['dbuser'],$config['dbpass'],$config['database']);
			
			//if authorized
			if($sess->login($uname,$pwd)) {
				echo "<p>Welcome $uname</p>";
				echo menu();
			} else {
				echo "You are not authorized";
			}
			?>	
		  </div>
		</div>
	  </div>
	</body>
</html>