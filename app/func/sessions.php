<?php
/*miscellaenous functions to be used*/
////just for saving sessions data about the user
 function getIPAddress() {
	return $_SERVER['REMOTE_ADDR'];
	
 }

 function getBrowser() {
 
	return $_SERVER['HTTP_USER_AGENT'];
	//echo $browserAgent;
 }

/* CLASS Sessions */
/* database backed sessions a la codeignite (but somewhat simpler) */
class Sessions {

	//to enable instant db connection within the class
	var $db;
	var $uname;
	var $timedelay = 600; //5minutes
	var $action;
	var $ipaddress;
	var $browser;
	
	public function __construct($uname,$serv,$user,$pwd,$dbase) {
		$this->uname = $uname;
		$this->db = new mysqli($serv,$user,$pwd,$dbase);
		
		
		
		
	}
	
	function startsessions($uname) {
		/*upon login . assumed session_start() was already called*/
		$_SESSION['username'] = $uname;
		$dbsql = "SELECT a.user_id, b.make_private, b.can_create, b.admin "; 
		$dbsql .= "FROM t_users a LEFT JOIN tbl_userlevels b ON a.user_id = b.user_id  
					WHERE a.username = '".$uname."'";
		
		//var_dump($dbsql); exit;
		$result = $this->db->query($dbsql);
		if(!$result) { echo $this->db->error; }
		if($result->num_rows > 0) {
			while($row = $result->fetch_array()) {
				$user_id =  $row['user_id'];
				$priv = $row['make_private']; 
				$create = $row['can_create'];
			}
		}
		$_SESSION['user_id'] = $user_id;
		
		$_SESSION['privateability'] = ($priv == 1) ? true : false;
	
		$_SESSION['createability']  = ($create == 1) ? true : false;
			
		
	}
	function destroysessions($uname,$redirect) {
		session_unset();
		//$redirecturl =  $redirect;
		
		//after destroying, display logged out page or go back to index. up to you
		header("Location: $redirect");
	}
	
	// login  check
	function login($uname, $password) {
		$login = $this->db->query("SELECT * FROM t_users WHERE t_users.username = '".$uname."' AND t_users.password = MD5('".$password."')");
		
		//echo "SELECT * FROM t_users WHERE t_users.username = '".$username."' AND t_users.password = MD5('".$password."')";
		//var_dump($login); exit;
		if(!$this->db->error) {
			$this->browser = getBrowser();
			$this->ipaddress = getIPAddress();
			$this->session_write($uname,$this->browser,$this->ipaddress);
			$this->startsessions($uname);
			//print_r($_SESSION);
			return true;
		}  else {
			return false;
		}
	}
	
	function logout($username) {
		//just delete the guy from database
	
	}
	//i.e. write/insert  new session
	function session_overwrite($sessid, $uname, $uagent ='', $ipaddress = '') {
		$query = "UPDATE tbl_sessions  `username` = '".addslashes($uname)."' WHERE `sess_id` = '".addslashes($sessid)."'";
		
		return $this->db->query($query);
	}
	
	//i.e. update
	function session_write($uname,$uagent= '',$ipaddress = '') {
		$totally_random_hash = md5(md5(uniqid()));//time() . rand(1,5000)
		$query = "INSERT INTO tbl_sessions VALUES ('" . $totally_random_hash . "','" . $uname . "','";
		$query .=  $uagent . "','" . $ipaddress . "');";
		
		return $this->db->query($query);
	}
	
	//simply delete from database table
	function session_del($username) {
		$q = 	"DELETE * FROM tbl_sessions WHERE username='". $username."'" ;
		$this->db->query($q);
	}
	
	function check($uname) {
		$timenow = time();
		
		$result = $this->db->query("SELECT last_activity FROM tbl_sessions WHERE username = '".$uname."'");
		
		if($result->num_rows() > 0) {
			while($row = $result->fetch_array()) {
				$time_ago = $row['last_activity'];
			} 
			
			if(($timenow - $time_ago) > $this->time_delay ) {
				$this->action = 'update';
				return 'U'; //yes must update
			} else {
				$this->action = false;
				return false;
			}
			
		} else {
			$this->action = 'insert';
			return 'I';
		}
		
	}
	
	
}
