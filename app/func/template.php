<?php
$title = '';
//define('URL','http://squawknet.net/team/');
//base_url (CI function)
function base_url() {
	return URL;
}

//templating
function template_top() {
	$site = array('title' => 'TeamworkPM', 
				'description' => 'Task Management');
	require_once('app/templates/header.php');
}
function template_bottom() {
		require_once('app/templates/footer.php');

}

function admin_menu() {
	$menu =  "<nav><ul>";
	$menu .= "<li><a href=\"?c=admin&create_user\">Add user</a></li>";
	$menu .= "<li><a href=\"?t=0\">Check unassigned</a></li>";
	
	$menu .= "<li><a href=\"?t=1\">View All</a></li>";
	$menu .= "<li><a href=\"?c=admin&f=create_task\">Create a task</a></li>";
	
	$menu .= "</ul></nav>";
	return $menu;
}

function mem_menu($abilities) {
	$menu = '<nav><ul>';
	if(isset($abilities['create'])) {
		$menu .= '<li><a href="?c=mem&f=create_task\">Create a task</a>"></li>';
	}
	
	$menu .= '<li><a href="?t=3">My  tasks</a></li>';
	$menu .= '<li><a href="?c=mem&f=logout">Logout</a></li>';
	$menu .= "</ul></nav>";
	return $menu;
	
}



