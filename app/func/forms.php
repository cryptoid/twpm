<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Form {

	//http://codeigniter.com/user_guide/general/urls.html
	//a copy of the CI helper "form_helper" put inside an object

	function _attributes_to_string($attributes, $formtag = FALSE)
	{
		if (is_string($attributes) AND strlen($attributes) > 0)
		{
			if ($formtag == TRUE AND strpos($attributes, 'method=') === FALSE)
			{
				$attributes .= ' method="post"';
			}

			if ($formtag == TRUE AND strpos($attributes, 'accept-charset=') === FALSE)
			{
				$attributes .= ' accept-charset="'.strtolower(config_item('charset')).'"';
			}

		return ' '.$attributes;
		}

		if (is_object($attributes) AND count($attributes) > 0)
		{
			$attributes = (array)$attributes;
		}

		if (is_array($attributes) AND count($attributes) > 0)
		{
			$atts = '';

			if ( ! isset($attributes['method']) AND $formtag === TRUE)
			{
				$atts .= ' method="post"';
			}

			if ( ! isset($attributes['accept-charset']) AND $formtag === TRUE)
			{
				$atts .= ' accept-charset="'.strtolower(config_item('charset')).'"';
			}

			foreach ($attributes as $key => $val)
			{
				$atts .= ' '.$key.'="'.$val.'"';
			}

			return $atts;
		}
	}
	
	
	function hidden($name, $value = '', $id='')
	{

		if (isset($id) && $id != '')
		{
			return '<input type="hidden" name="'.$name.'" value="'.$value. '" id="'.$id.'" />'."\n";
		} else {
			return '<input type="hidden" name="'.$name.'" value="'.$value. '" />'."\n";
		}
	}
		
	function input($name, $value = '', $id='')
	{

		if($id == '') { $noid = ''; } else { $noid = "id=\"".$id."\""; }
		return '<input type="text" name="'.$name.'" value="'.$value. '" '.$noid. '/>'."\n";

	}
	//has a max length if needed
	function limited_input($name, $value = '', $id='',$limited) {
			if($id == '') { $noid = ''; } else { $noid = "id=\"".$id."\""; }
		return '<input type="text" name="'.$name.'" value="'.$value. '" '.$noid.  ' maxlength="'.$limited.'" />'."\n";

	}
	
	function open($action = '', $attributes = '', $hidden = array())
	{
		

		if ($attributes == '')
		{
			$attributes = 'method="post"';
		}

		// If an action is not a full URL then turn it into one
		if ($action && strpos($action, '://') === FALSE)
		{
			$action = base_url() . $action; //CI->config-site_url($action);
		}

		// If no action is provided then set to the current url
		if(empty($action)) {
			$action = $_SERVER['URI']; //$CI->config->site_url
		}
		
		$form = '<form action="'.$action.'"';

		$form .= $this->_attributes_to_string($attributes, TRUE);

		$form .= '>';

		if (is_array($hidden) AND count($hidden) > 0)
		{
			$form .= sprintf("<div style=\"display:none\">%s</div>", form_hidden($hidden));
		}

		return $form;
	}
	
	

}

/* End of file forms.php */
/* Location: app/func/forms.php */