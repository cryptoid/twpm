<?php
//known as the router / dispatch. very simple
if(isset($_GET['c']) && isset($_GET['f'])) {
	$controller = $_GET['c'];
	$func = $_GET['f'];
	
	//controller will just be the folder
	//func is the actual php file
	require_once($controller . "/" . $func . ".php");
}

