==README==
==========

the default username is 'joe' and password is 'test'. 

*sql code* is at `twpm.sql`

setup: 
------

go to `app/config/database.php`

set your database settings there. (yup, same as in codeignite, because I have a CI app and I just pointed it to use the same file.)

set `app/definitions.php` - these are define statements for the URL of this application

requirements:
-------------

I implemented mysqli connections through the *Sessions class* in `app/func/sessions.php`, thus you need a PHP with mySQLi extension enabled.

why something too simple?
-------------------------
I dont code complicated things and have no time to do so. 

In most projects I have been involved in, I'm just asked the estimates to complete. 

If I don't meet it, we simply add more time to original estimate.

I just did this for fun actually. 

Actually I plan to use this to manage my mini pet projects with a team that is just in a facebook group. 

why are there some codeignite files around?
--------------------------------------
you may remove them eexcept for the app/config/database.php and app/definitions.php and the folders app/func, app/templates

as to why i didn't use codeigniter to do the whole app - i just felt like using  my own skills to create a simpler framework.

Why should I use this?
----------------------

Don't use if you have to measure velocity, or need a tool to estimate future tasks completion and assign story points to tasks. 

There's free Pivotal Tracker by Pivotal, or JIRA by Atlassian. 

Those project management / task management systems which have all the bells and whistles you need. 

Do use if you want to host your own PM solution and need something very,very simple.