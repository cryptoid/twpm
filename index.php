<?php
	include_once('app/definitions.php');
	include_once('app/config/database.php');
	include_once('app/dbconn.php');
	
	include_once('app/func/template.php');
	include_once('app/func/router.php');
	include_once('app/func/sessions.php');
	
	if(!isset($_GET['c']) && !isset($_GET['f'])) {
?>
	<body>
	<div class="container">
		  <div class="row">
		  <div class="span8">
		  <?php
		  if(isset($_GET['t'])) {
			switch($_GET['t']) {
				//view unassigned only available to admin
				case 0:
					$dbquery = "SELECT * FROM tasks WHERE assignedto = 'admin'";
				break;
				//view all tasks which aren't assignedto admin  will be viewable by admin and member
				case 1:
					$dbquery = "SELECT * FROM tasks WHERE assignedto != 'admin' ";
				break;
				
				//view my tasks = only assigned to = me. only for members
				case 3:
					$dbquery = "SELECT * FROM tasks WHERE assignedto != 'admin' AND assignedto == '". $_SESSION['username'] ."'"; 
				break;
			}
				$dbconn->query($dbquery);
		  } else {
			//just show the welcome message.
		  ?>
		  
			<h1>Welcome to our TeamWorkPM!</h1>
			
			<p>Tasks as the basic unit of movement instead of stories</p>
			<ul>
				<li>Simplicity over Complexity</li>
				<li>Functional Specs divided into tiny tasks  instead of feature driven approach</li>
			</ul>
			
			<p><a href="contact.php">Contact</a></p>
			<?php
			}
			?>
		  </div>
		  <div class="span4">
				<form action="index.php?c=mem&f=login" method="post" accept-charset="utf-8">          
			  <label>Username</label>
			  <p><input type="text" name="username" value=""  /></p>
			  
			  <label>Password</label>
			  <p><input type="password" name="password" value=""  /></p>
			  
				<input type="submit" name="submit" value="Login">
			  
						</form> 
		  </div>
		</div>
	  </div>
	</body>
</html>
<?php
} 
?>