-- Adminer 3.3.4 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `t_users`;
CREATE TABLE `t_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_number` varchar(19) NOT NULL,
  `enabled` int(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `t_users` (`user_id`, `username`, `password`, `contact_email`, `contact_number`, `enabled`) VALUES
(1,	'joe',	'098f6bcd4621d373cade4e832627b4f6',	'some@example.com',	'1234',	1);

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` text NOT NULL,
  `assignedto` varchar(100) NOT NULL,
  `assignedby` varchar(100) NOT NULL,
  `estimate` varchar(25) NOT NULL,
  `public` int(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tasks` (`id`, `task`, `assignedto`, `assignedby`, `estimate`, `public`, `created`, `modified`) VALUES
(1,	'some task',	'jpalala',	'admin',	'3d',	0,	'2012-11-22 13:12:45',	'2012-11-22 13:12:45');

DROP TABLE IF EXISTS `tbl_sessions`;
CREATE TABLE `tbl_sessions` (
  `sess_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `username` text NOT NULL,
  PRIMARY KEY (`sess_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tbl_userlevels`;
CREATE TABLE `tbl_userlevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `make_private` int(1) NOT NULL,
  `can_create` int(1) NOT NULL,
  `admin` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_userlevels` (`id`, `user_id`, `make_private`, `can_create`, `admin`) VALUES
(1,	1,	1,	1,	1);

-- 2012-11-27 22:52:35